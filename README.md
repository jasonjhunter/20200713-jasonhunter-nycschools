
https://gitlab.com/jasonjhunter/20200713-jasonhunter-nycschools

This repository contains a native Java/Kotlin hybrid Android application providing information on NYC High schools as per the coding challenge set forth by JPMC.  This app implements a Model-View-ViewModel architecture using Koin dependency injection framework, Retrofit with OkHttp clients for accessing RESTful Web APIs, GSON for data serialization, Timber for logging and Android Lifecycle and Architectural Components to achieve clean MVVM design.

The focus of this lightweight app is to demonstrate the use and understanding SOLID programming principles and the use of modern design and architecture.  The app is intentionally lightweight, and as such does not place emphasis on UI design, test-driven development, or in-depth error handling that a normal enterprise application would implement.  The app does follow tiered and fragment architecture principles, separation of concerns, interface segregation, and dependency inversion implementation.


