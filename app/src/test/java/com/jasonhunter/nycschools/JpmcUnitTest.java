package com.jasonhunter.nycschools;

import com.jasonhunter.nycschools.service.RestInterface;
import com.jasonhunter.nycschools.service.ServiceGenerator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class JpmcUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void activities_shouldNotBeNull() {
        String itemListActivity = new String();
        assertNotNull(itemListActivity);

        String itemDetailActivity = new String();
        assertNotNull(itemDetailActivity);
    }

    @Test
    public void serviceGenerator_shouldNotBeNull() {

        RestInterface serviceGenerator =
                ServiceGenerator.createService(RestInterface.class);

        assertNotNull(serviceGenerator);
    }

}
