package com.jasonhunter.nycschools.model.school;

import com.google.gson.annotations.SerializedName;

/**
 * High school class
 * Extends abstract base School class
 */
public class HighSchool extends School {

    @SerializedName("schoolName")
    private String schoolName;

    @SerializedName("boro")
    private String boro;

    @SerializedName("overviewParagraph")
    private String overviewParagraph;

    @SerializedName("school_email")
    private String schoolEmail;

    @SerializedName("website")
    private String website;

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getBoro() {
        return boro;
    }

    public void setBoro(String boro) {
        this.boro = boro;
    }

    public String getOverviewParagraph() {
        return overviewParagraph;
    }

    public void setOverviewParagraph(String overviewParagraph) {
        this.overviewParagraph = overviewParagraph;
    }

    public String getSchoolEmail() {
        return schoolEmail;
    }

    public void setSchoolEmail(String schoolEmail) {
        this.schoolEmail = schoolEmail;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     * constructor with arguments for instantiating object with immediate property initialization (optional)
     */
    public HighSchool(String dbn, String schoolName, String boro, String overviewParagraph, String schoolEmail, String website) {
        super.setDbn(dbn);
        this.schoolName = schoolName;
        this.boro = boro;
        this.schoolEmail = schoolEmail;
        this.website = website;
    }
}
