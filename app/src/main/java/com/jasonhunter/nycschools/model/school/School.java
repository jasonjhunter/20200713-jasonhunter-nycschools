package com.jasonhunter.nycschools.model.school;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * School abstract base class
 * Implements Serializable interface
 */
public abstract class School implements Serializable {

    @SerializedName("dbn")
    private String dbn;

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }
}
