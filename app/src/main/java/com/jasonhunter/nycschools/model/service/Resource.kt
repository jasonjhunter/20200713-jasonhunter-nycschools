package com.jasonhunter.nycschools.model.service

import com.jasonhunter.nycschools.Constants.Status

// ** This file comes from the Google's Architecture Component Samples repository **
// Implementing this class allows us to signal the current state to the caller, be that success or error.

data class Resource<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): Resource<T> {
            return Resource(Status.ERROR, data, msg)
        }

        fun <T> serviceError(msg: String, data: T?): Resource<T> {
            return Resource(Status.SERVICE_ERROR, data, msg)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }

        private val TAG = Resource::class.java.simpleName
    }
}