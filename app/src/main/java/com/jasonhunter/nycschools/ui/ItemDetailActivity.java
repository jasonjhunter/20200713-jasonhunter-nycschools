package com.jasonhunter.nycschools.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.jasonhunter.nycschools.R;
import com.jasonhunter.nycschools.model.school.HighSchool;

import java.io.Serializable;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class ItemDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        if (savedInstanceState == null) {
            /**
             * Create the fragment, add it to the activity
             * Here we are passing the entire High School Object in the bundle, this is an
             * alternative to simply passing a unique ID, then having to make an additional
             * unnecessary network request/database call or filtering operation within ItemDetailFragment
             */
            if (getIntent().hasExtra(ItemDetailFragment.ARG_ITEM)) {
                Serializable itemSerializable = getIntent().getSerializableExtra(ItemDetailFragment.ARG_ITEM);
                if (itemSerializable != null) {
                    HighSchool item = (HighSchool) itemSerializable;
                    if (item != null) {
                        Bundle arguments = new Bundle();
                        arguments.putSerializable(ItemDetailFragment.ARG_ITEM, item);
                        //ItemDetailFragment fragment = new ItemDetailFragment();
                        //fragment.setArguments(arguments);
                        ItemDetailFragment itemDetailsFragment = ItemDetailFragment.newInstance(item);
                        getSupportFragmentManager().beginTransaction()
                                .add(R.id.item_detail_container, itemDetailsFragment)
                                .commit();
                    }
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            navigateUpTo(new Intent(this, ItemListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
