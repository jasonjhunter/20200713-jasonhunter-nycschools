package com.jasonhunter.nycschools.ui

import androidx.lifecycle.*
import com.jasonhunter.nycschools.model.school.HighSchool
import com.jasonhunter.nycschools.model.service.Resource
import com.jasonhunter.nycschools.service.ServiceApi
import kotlinx.coroutines.Dispatchers
import timber.log.Timber

class ItemViewModel(private val serviceApi: ServiceApi) : ViewModel() {

    private val _errorMessage = MutableLiveData<Exception>()
    val errorMessage: LiveData<Exception> = _errorMessage

    private val _highSchoolList = MutableLiveData<List<HighSchool>>()
    val highSchoolList: LiveData<List<HighSchool>> = _highSchoolList

    private val _getHighSchools = MutableLiveData<String>()

    fun getHighSchools(input: String) {
        _getHighSchools.value = input  // Trigger the value change
    }

    var getHighSchoolsResponse: LiveData<Resource<List<HighSchool>>> =
            _getHighSchools.switchMap { optional_param_value ->
                liveData(Dispatchers.IO) {                // Create a new co-routine to move the execution off the UI thread
                    emit(Resource.loading(null))          // executed on a thread reserved for I/O operations - First emits loading status
                    try {
                        val result = serviceApi.getHighSchools(optional_param_value)
                        emit(result)  // After the API returns a value, it will be emitted to the the observer(s)
                        _highSchoolList.postValue(result.data)     // set the variable value
                    } catch (e: Exception) {
                        emit(Resource.error(e.localizedMessage, null))
                        Timber.e(e)
                        _errorMessage.postValue(e)
                    }
                }
            }
}