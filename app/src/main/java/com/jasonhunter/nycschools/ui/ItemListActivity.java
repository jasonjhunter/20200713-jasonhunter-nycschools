package com.jasonhunter.nycschools.ui;

import android.os.Bundle;

import com.jasonhunter.nycschools.Constants;
import com.jasonhunter.nycschools.R;
import com.jasonhunter.nycschools.model.school.HighSchool;
import com.jasonhunter.nycschools.model.service.Resource;
import com.jasonhunter.nycschools.service.ResponseHandler;
import com.jasonhunter.nycschools.service.RestInterface;
import com.jasonhunter.nycschools.service.ServiceApi;
import com.jasonhunter.nycschools.service.ServiceGenerator;

import java.io.Serializable;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

/**
 * An activity representing a list of NYC High Schools. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of schools, which when touched,
 * lead to a [ItemDetailActivity] representing
 * school details. On tablets, the activity presents the list of schools and
 * school details side-by-side using two vertical panes.
 */
public class ItemListActivity extends BaseActivity {

    private static final String HIGH_SCHOOLS = "high_schools";

    private boolean mTwoPane;   // Whether or not the activity is in two-pane mode, i.e. running on a tablet
    private List<HighSchool> highSchools;
    private ItemViewModel itemViewModel;
    // provide both required dependencies (ResponseHandler & RestInterface) to ServiceApi
    ServiceApi serviceApi = new ServiceApi(new ResponseHandler(),
            ServiceGenerator.createService(RestInterface.class));
    // initialize ItemViewModelFactory providing required serviceApi dependency
    private ItemViewModelFactory itemViewModelFactory = new ItemViewModelFactory(serviceApi);

    //  highSchoolListObserver observer initialization
    private Observer highSchoolListObserver = (Observer<Resource<List<HighSchool>>>) it -> {
        if (it.getStatus() == Constants.Status.SUCCESS) {
            setupRecyclerView(it.getData());
        } else if (it.getStatus() == Constants.Status.ERROR) {
            showError(
                    getString(R.string.jpmc_error, it.getMessage()), findViewById(R.id.progressbar_loading));
        } else if (it.getStatus() == Constants.Status.LOADING) {
            showLoading(getString(R.string.jpmc_loading), findViewById(R.id.progressbar_loading));
        }
    };

    //  errorObserver observer initialization
    private Observer errorObserver = (Observer<Resource<List<HighSchool>>>) it -> {
        showError(
                getString(R.string.jpmc_error, it.getMessage()), findViewById(R.id.progressbar_loading));
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        if (findViewById(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        // initialize itemViewModel via ViewModelProvider, supplying required lifecycle & itemViewModelFactory
        itemViewModel = new ViewModelProvider(this, itemViewModelFactory).get(ItemViewModel.class);

        // attach observers to ItemViewModel
        itemViewModel.getGetHighSchoolsResponse().observe(this, highSchoolListObserver);
        itemViewModel.getErrorMessage().observe(this, errorObserver);

        try {
            if (itemViewModel.getHighSchoolList().getValue() != null && !itemViewModel.getHighSchoolList().getValue().isEmpty()) {
                // first check the view model to see if it has retained state
                highSchools = itemViewModel.getHighSchoolList().getValue();
                setupRecyclerView(highSchools);
            } else if (savedInstanceState != null && savedInstanceState.containsKey(HIGH_SCHOOLS)) {
                // check the savedInstanceState to see if it has retained state (view model should of been sufficient)
                highSchools = (List<HighSchool>) savedInstanceState.getSerializable(HIGH_SCHOOLS);
                setupRecyclerView(highSchools);
            } else {
                itemViewModel.getHighSchools("optional_param_value");   // make network call
            }
        } catch (Exception e) {
            System.out.println(e.toString());   // TODO: More robust error logging
            showError(e.toString(), findViewById(R.id.progressbar_loading));
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        // save highSchools in the bundle for potential access later. IE. configuration change/rotation
        outState.putSerializable(this.HIGH_SCHOOLS, (Serializable) highSchools);
    }

    private void setupRecyclerView(List<HighSchool> list) {

        hideLoading(findViewById(R.id.progressbar_loading));

        ItemRecyclerViewAdapter adapter = new ItemRecyclerViewAdapter(this, list, mTwoPane);

        if (list != null) {
            if (!list.isEmpty()) {
                RecyclerView recyclerView = findViewById(R.id.item_list);
                if (recyclerView != null) {
                    recyclerView.setAdapter(adapter);
                }
            }
        }
    }
}