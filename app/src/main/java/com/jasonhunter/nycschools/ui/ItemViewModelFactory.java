package com.jasonhunter.nycschools.ui;

import com.jasonhunter.nycschools.service.ServiceApi;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

/**
 * An utility class that provides the ItemViewModel
 * Default {@code ViewModelProvider} for an {@code Activity} or a {@code Fragment} can be obtained
 */
@SuppressWarnings("WeakerAccess")
public class ItemViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    @NonNull
    private final ServiceApi serviceApi;

    public ItemViewModelFactory(@NonNull ServiceApi serviceApi) {
        this.serviceApi = serviceApi;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass == ItemViewModel.class) {
            return (T) new ItemViewModel(serviceApi);
        }
        return null;
    }
}