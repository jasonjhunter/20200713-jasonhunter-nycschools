package com.jasonhunter.nycschools.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jasonhunter.nycschools.R;
import com.jasonhunter.nycschools.model.school.HighSchool;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class ItemRecyclerViewAdapter
        extends RecyclerView.Adapter<ItemRecyclerViewAdapter.ViewHolder> {

    private final ItemListActivity mParentActivity;
    private final List<HighSchool> mValues;
    private final boolean mTwoPane;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            HighSchool item = (HighSchool) view.getTag();
            if (mTwoPane) {
                /* Creating a new bundle, and adding the entire High School object item
                 * as a bundle argument
                 * Here we are passing the entire High School Object in the bundle, this is an
                 * alternative to simply passing a unique ID, then having to make an additional
                 * unnecessary network request/database call or filtering operation within ItemDetailFragment
                 */
                //Bundle arguments = new Bundle();
                //arguments.putSerializable(ItemDetailFragment.ARG_ITEM, item);
                ItemDetailFragment itemDetailsFragment = ItemDetailFragment.newInstance(item);
                ItemDetailFragment fragment = new ItemDetailFragment();
                //fragment.setArguments(arguments);
                mParentActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.item_detail_container, itemDetailsFragment)
                        .commit();

                mParentActivity.setTitle(item.getSchoolName()); // Update toolbar title
            } else {
                Context context = view.getContext();
                Intent intent = new Intent(context, ItemDetailActivity.class);
                intent.putExtra(ItemDetailFragment.ARG_ITEM, item);

                context.startActivity(intent);
            }
        }
    };

    ItemRecyclerViewAdapter(ItemListActivity parent,
                            List<HighSchool> items,
                            boolean twoPane) {
        mValues = items;
        mParentActivity = parent;
        mTwoPane = twoPane;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mIdView.setText(mValues.get(position).getDbn());
        holder.mContentView.setText(mValues.get(position).getSchoolName());

        holder.itemView.setTag(mValues.get(position));
        holder.itemView.setOnClickListener(mOnClickListener);
        //holder.itemView.setOnClickListener((View v) -> { handleClick(mValues.get(position));/* lambda handle click here*/  });
    }

    // Just an example of another way to add a clickListener
    //private void handleClick(HighSchool highSchool) {  displayHighSchoolDetail(highSchool);  }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView mIdView;
        final TextView mContentView;

        ViewHolder(View view) {
            super(view);
            mIdView = (TextView) view.findViewById(R.id.id_text);
            mContentView = (TextView) view.findViewById(R.id.content);
        }
    }
}
