package com.jasonhunter.nycschools.ui;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {

    /**
     * Shows the loading progress
     *
     * @return void
     * @params: message, progressBar
     */
    protected void showLoading(
            String message,
            ProgressBar progressBar
    ) {
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Hides the loading progress
     *
     * @return void
     * @params: progressBar
     */
    protected void hideLoading(ProgressBar progressBar) {
        progressBar.setVisibility(View.GONE);
    }

    /**
     * Shows error messages
     *
     * @return void
     * @params: message, progressBar
     */
    protected void showError(
            String message,
            ProgressBar progressBar
    ) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        hideLoading(progressBar);
    }
}
