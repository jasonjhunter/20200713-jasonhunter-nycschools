package com.jasonhunter.nycschools.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.jasonhunter.nycschools.R;
import com.jasonhunter.nycschools.model.school.HighSchool;

import androidx.fragment.app.Fragment;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends Fragment {
    /**
     * The fragment argument representing the school this fragment is presenting
     */
    public static final String ARG_ITEM = "item";

    /**
     * The school content this fragment is presenting
     */
    private HighSchool mItem;

    /**
     * newInstance Convenience method
     *
     * @return ItemDetailFragment
     * @params: HighSchool
     */
    public static ItemDetailFragment newInstance(HighSchool val) {
        ItemDetailFragment itemDetailFragment = new ItemDetailFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARG_ITEM, val);
        itemDetailFragment.setArguments(args);

        return itemDetailFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            if (getArguments().containsKey(ARG_ITEM)) {
                // Load the content specified by the fragment
                mItem = (HighSchool) getArguments().getSerializable(ARG_ITEM);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);

        // Show the content as text in a TextView
        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.item_detail)).setText(mItem.getOverviewParagraph());
            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.getSchoolName());   // single pane view
            }
        }

        return rootView;
    }
}
