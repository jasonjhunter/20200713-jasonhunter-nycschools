package com.jasonhunter.nycschools

import android.app.Application
import android.content.Context
import android.util.Log
import timber.log.Timber

class App : Application() {

    private val instance: App = this.getInstance()

    override fun onCreate() {
        super.onCreate()

        Log.i(TAG, "App initialization")

        Timber.plant(Timber.DebugTree());
 
        Companion.appContext = applicationContext
    }

    fun getInstance(): App {
        return instance
    }

    companion object {

        private val TAG = App::class.java.simpleName

        private lateinit var appContext: Context

        fun getAppContext(): Context {
            return appContext
        }
    }
}