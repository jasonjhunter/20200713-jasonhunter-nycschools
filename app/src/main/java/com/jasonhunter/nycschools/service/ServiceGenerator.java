package com.jasonhunter.nycschools.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jasonhunter.nycschools.BuildConfig;

import java.text.DateFormat;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * creates a new REST client with the given API base url.
 */
public class ServiceGenerator {

    private static String apiBaseUrl = BuildConfig.API_URL;
    private static String apiAppToken = BuildConfig.API_APP_TOKEN;
    private static Retrofit.Builder retrofitBuilder = initRetrofitBuilder();
    public static Retrofit retrofit = retrofitBuilder.build();

    public static <S> S createService(Class<S> apiInterface) {

        return retrofit.create(apiInterface);
    }

    private static Retrofit.Builder initRetrofitBuilder() {

        Retrofit.Builder newRetrofitBuilder = new Retrofit.Builder()
                .baseUrl(apiBaseUrl)
                .addConverterFactory(getConverterFactory())
                .client(getOkHttpClient());

        return newRetrofitBuilder;
    }

    private static Converter.Factory getConverterFactory() {

        Gson gsonBuilder = new GsonBuilder()
                .serializeNulls()
                .setDateFormat(DateFormat.LONG)
                .setPrettyPrinting()
                .create();

        return GsonConverterFactory.create(gsonBuilder);
    }

    private static OkHttpClient getOkHttpClient() {

        // 1.) create an OkHttpClient Builder (set appropriate properties)
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder()
                .connectTimeout(30 * 1000, TimeUnit.SECONDS)
                .readTimeout(30 * 1000, TimeUnit.SECONDS)
                .writeTimeout(30 * 1000, TimeUnit.SECONDS);

        // 2.) add the interceptors to the builder
        OkHttpClient.Builder okHttpClientBuilderWithInterceptors = addInterceptors(okHttpClientBuilder);

        // 3.) build the builder (aka: create the okHttpClient)
        OkHttpClient okHttpClient = okHttpClientBuilderWithInterceptors.build();

        // 4.) return the okHttpClient
        return okHttpClient;
    }

    private static OkHttpClient.Builder addInterceptors(OkHttpClient.Builder okHttpClientBuilder) {

        HttpLoggingInterceptor.Level level = HttpLoggingInterceptor.Level.BODY;
        if (!BuildConfig.DEBUG) {
            level = HttpLoggingInterceptor.Level.NONE;   // do not log in Release builds
        }

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(level);
        if (!okHttpClientBuilder.interceptors().contains(logging))
            okHttpClientBuilder.addInterceptor(logging);

        Interceptor appTokenInterceptor = chain -> {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("X-App-Token", apiAppToken)
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        };

        if (!okHttpClientBuilder.interceptors().contains(appTokenInterceptor))
            okHttpClientBuilder.addInterceptor(appTokenInterceptor);

        return okHttpClientBuilder;
    }

}