package com.jasonhunter.nycschools.service

import com.jasonhunter.nycschools.model.school.HighSchool
import com.jasonhunter.nycschools.model.service.Resource
import retrofit2.Response
import timber.log.Timber

// Note: In a real world scenario, there would be a lot more
// error catching/handling in this class and throughout the entire app


class ServiceApi(private val responseHandler: ResponseHandler, private val restInterface: RestInterface) {

    /**
     * Called from our view model to retrieve the list of NYS High Schools
     * fields entered).
     *
     * @return Resource<List<HighSchool>>
     */
    fun getHighSchools(id: String): Resource<List<HighSchool>> {

        Timber.d("getHighSchools")

        // TODO: introduce more robust error handling
        // Note: id is currently not being employed
        try {

            val response: Response<List<HighSchool>> =
                    restInterface.highSchools.execute()

            if (response.errorBody() != null) {
                throw Exception(response.raw().toString())
            } else {
                var responseBody = response.body()

                Timber.d(
                        "\nresponse code : ${response.code()}\n" +
                                "response body : ${response.body()}"
                )

                return responseHandler.handleSuccess(responseBody!!)
            }
        } catch (e: Exception) {

            Timber.e(e)

            return responseHandler.handleException(e)
        }
    }
}
