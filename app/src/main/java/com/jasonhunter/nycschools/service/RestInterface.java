package com.jasonhunter.nycschools.service;

import com.jasonhunter.nycschools.model.school.HighSchool;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

// Definition of REST endpoints
public interface RestInterface {

    /**
     * Called from our ServiceApi
     *
     * @return List<HighSchool>
     */
    //@Headers("X-App-Token: kkPNyhJKdt7XSCkw11VEElJIP")  // Note: could also add header values here
    @GET("uq7m-95z8.json?$select=dbn,boro,website,overview_paragraph%20AS%20overviewParagraph,school_email%20AS%20schoolEmail,school_name%20AS%20schoolName&$order=school_name")
    Call<List<HighSchool>> getHighSchools();

    /*
     * Here, I  used their "Socrata Query Language" (SoQL) to pair down the data to just what needed
     *
     * - used the keyword "Select" to only grab the columns  I am requiring
     * - used column Aliases to alias and underscored property names - IE school-underscore-name to schoolName
     * - used the key word "Order" to sort the schools alphabetically, by school name
     *
     * There was just over 400 records, so for this example, I kept it simple, did not implement
     * any Paging with limits and offsets IE. $limit=100&offset=100
     *
     * - used an application token to reduce throttling.
     */

}
