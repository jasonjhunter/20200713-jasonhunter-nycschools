package com.jasonhunter.nycschools.service

import com.jasonhunter.nycschools.model.service.Resource
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

enum class ErrorCodes(val code: Int) {
    SocketTimeOut(-1),
    UnknownHostException(0)
}

// ResponseHandler class, responsible for wrapping both successful and failed responses
// Note: to make a class inheritable to the other classes, you must mark it with the 'open' keyword
open class ResponseHandler {

    // simply wraps the given data
    fun <T : Any> handleSuccess(data: T): Resource<T> {
        return Resource.success(data)
    }

    // resolves an error message and returns that
    fun <T : Any> handleException(e: Exception): Resource<T> {
        return when (e) {

            is HttpException -> Resource.error(getErrorMessage(e.code(), e.message), null)

            is SocketTimeoutException -> Resource.error(
                    getErrorMessage(
                            ErrorCodes.SocketTimeOut.code, e.message
                    ), null
            )

            is UnknownHostException -> Resource.error(
                    getErrorMessage(
                            ErrorCodes.UnknownHostException.code, e.message
                    ), null
            )

            else -> Resource.error(getErrorMessage(Int.MAX_VALUE, e.message), null)
        }
    }

    private fun getErrorMessage(code: Int, message: String?): String {
        return when (code) {
            ErrorCodes.SocketTimeOut.code -> "Timeout"
            ErrorCodes.UnknownHostException.code -> message ?: "UnknownHost"
            204 -> message ?: "No Content"
            401 -> message ?: "Unauthorized"
            404 -> message ?: "Not found"
            else -> message ?: "Something not specifically being caught went wrong"
            // If the expression to the left of ?: is not null, return it
        }
    }

    companion object {

        private val TAG = ResponseHandler::class.java.simpleName
    }
}
